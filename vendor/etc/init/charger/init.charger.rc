import /vendor/etc/init/charger/init.startoff_usbcharger.rc
import /vendor/etc/init/hw/init.qti.ufs.rc
import /vendor/etc/init/hw/init.qcom.usb.rc
import /vendor/etc/init/hw/init.qcom.test.rc
import /vendor/etc/init/hw/init.target.rc
import /vendor/etc/init/hw/init.qcom.factory.rc
import /vendor/etc/init/bms_soc.rc
import /vendor/etc/init/usb_port.rc
import /vendor/etc/init/bms_thermal.rc
import /vendor/etc/init/bms_auth.rc
import /vendor/etc/init/bms_protocol.rc
import /vendor/etc/init/bms_logcat.rc
import /vendor/etc/init/vendor.qti.adsprpc-guestos-service.rc
import /vendor/etc/init/vendor.qti.audio-adsprpc-service.rc

on early-init
    # Mount binderfs
    mkdir /dev/binderfs
    mount binder binder /dev/binderfs stats=global
    chmod 0755 /dev/binderfs
    symlink /dev/binderfs/binder /dev/binder
    symlink /dev/binderfs/hwbinder /dev/hwbinder
    symlink /dev/binderfs/vndbinder /dev/vndbinder
    chmod 0666 /dev/binderfs/hwbinder
    chmod 0666 /dev/binderfs/binder
    chmod 0666 /dev/binderfs/vndbinder

    chmod 0644 /dev/adsp_misc
    chown audioserver audioserver /dev/adsp_misc

    # Set up linker config subdirectories based on mount namespaces
    mkdir /linkerconfig/bootstrap 0755
    mkdir /linkerconfig/default 0755
    # Generate ld.config.txt for early executed processes
    exec -- /system/bin/linkerconfig --target /linkerconfig/bootstrap
    chmod 644 /linkerconfig/bootstrap/ld.config.txt
    copy /linkerconfig/bootstrap/ld.config.txt /linkerconfig/default/ld.config.txt
    chmod 644 /linkerconfig/default/ld.config.txt
    # Mount bootstrap linker configuration as current
    mount none /linkerconfig/bootstrap /linkerconfig bind rec

    # Android doesn't need kernel module autoloading, and it causes SELinux
    # denials.  So disable it by setting modprobe to the empty string.  Note: to
    # explicitly set a sysctl to an empty string, a trailing newline is needed.
    write /proc/sys/kernel/modprobe \n

    # Set the security context of /postinstall if present.
    restorecon /postinstall
    # Set init and its forked children's oom_adj.
    write /proc/1/oom_score_adj -1000
    write /proc/sys/kernel/sysrq 0
    restorecon /adb_keys
    mkdir /mnt 0775 root system
    start ueventd

    # Run apexd-bootstrap so that APEXes that provide critical libraries
    # become available. Note that this is executed as exec_start to ensure that
    # the libraries are available to the processes started after this statement.
    exec_start apexd-bootstrap

on init
    #USB controller configuration
    setprop vendor.usb.rndis.func.name "gsi"
    setprop vendor.usb.rmnet.func.name "gsi"
    setprop vendor.usb.rmnet.inst.name "rmnet"
    setprop vendor.usb.dpl.inst.name "dpl"
    setprop vendor.usb.qdss.inst.name "qdss_mdm"
    setprop vendor.usb.controller a600000.dwc3

    #export PATH /sbin:/system/bin
    export ANDROID_ROOT /system
    export ANDROID_DATA /data
    export EXTERNAL_STORAGE /sdcard
    sysclktz 0

    copy /proc/cmdline /dev/urandom
    copy /default.prop /dev/urandom
    symlink /system/etc /etc
    symlink /sys/kernel/debug /d
    mkdir /config 0500 root root

    # we should use /dev/graphic/fb0 to display
    rm /dev/dri
    write /proc/sys/kernel/printk_level 6
    write /proc/sys/kernel/panic_on_oops 1
    write /proc/sys/vm/max_map_count 1000000
    symlink /system/vendor /vendor
    chown system system /dev/cpuctl
    chown system system /dev/cpuctl/tasks

on charger
    mkdir /log
    mount ext4 /dev/block/bootdevice/by-name/log /log
    restorecon /log
    chmod 775 /log
    chown root system /log

    exec_start apexd-charger

    write /sys/kernel/boot_adsp/boot 1
    wait /sys/class/power_supply/battery
    wait /sys/class/power_supply/usb

    chmod 0660 /sys/class/hw_power/charger/charge_data/iin_thermal
    chown system system /sys/class/hw_power/charger/charge_data/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc4/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger_sc4/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger_sc/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger/iin_thermal
    chmod 0664 /sys/class/power_supply/bms/chg_cycle_count
    chown system system /sys/class/power_supply/bms/chg_cycle_count
    chmod 0660 /sys/class/power_supply/bms/reset_learned_cc
    chown system system /sys/class/power_supply/bms/reset_learned_cc
    chmod 0660 /sys/class/power_supply/bms/voltage_max
    chown system system /sys/class/power_supply/bms/voltage_max
    chmod 0660 /sys/class/power_supply/battery/constant_charge_current_max
    chown system system /sys/class/power_supply/battery/constant_charge_current_max
    chmod 0660 /sys/class/hw_power/adapter/test_result
    chown system system /sys/class/hw_power/adapter/test_result
    chmod 0660 /sys/class/hw_power/charger/charge_data/ichg_ratio
    chown system system /sys/class/hw_power/charger/charge_data/ichg_ratio
    chmod 0660 /sys/class/hw_power/charger/charge_data/vterm_dec
    chown system system /sys/class/hw_power/charger/charge_data/vterm_dec
    chmod 0660 /sys/class/hw_power/interface/enable_charger
    chown system system /sys/class/hw_power/interface/enable_charger
    chmod 0660 /sys/class/hw_power/interface/ichg_limit
    chown system system /sys/class/hw_power/interface/ichg_limit
    chmod 0660 /sys/class/hw_power/interface/ichg_ratio
    chown system system /sys/class/hw_power/interface/ichg_ratio
    chmod 0660 /sys/class/hw_power/interface/vterm_dec
    chown system system /sys/class/hw_power/interface/vterm_dec
    chmod 0660 /sys/class/hw_power/interface/enable_debug
    chown system system /sys/class/hw_power/interface/enable_debug
    chmod 0222 /sys/class/hw_power/soc_decimal/start
    chown system system /sys/class/hw_power/soc_decimal/start
    chmod 0444 /sys/class/hw_power/soc_decimal/soc
    chown system system /sys/class/hw_power/soc_decimal/soc
    chmod 0444 /sys/class/hw_power/power_ui/cable_type
    chown system system /sys/class/hw_power/power_ui/cable_type
    chmod 0444 /sys/class/hw_power/power_ui/icon_type
    chown system system /sys/class/hw_power/power_ui/icon_type
    chmod 0444 /sys/class/hw_power/power_ui/max_power
    chown system system /sys/class/hw_power/power_ui/max_power
    chmod 0444 /sys/class/hw_power/power_ui/inner_max_power
    chown system system /sys/class/hw_power/power_ui/inner_max_power
    chmod 0444 /sys/class/hw_power/power_ui/wl_off_pos
    chown system system /sys/class/hw_power/power_ui/wl_off_pos
    chmod 0444 /sys/class/hw_power/power_ui/wl_fan_status
    chown system system /sys/class/hw_power/power_ui/wl_fan_status
    chmod 0444 /sys/class/hw_power/power_ui/wl_cover_status
    chown system system /sys/class/hw_power/power_ui/wl_cover_status
    chmod 0444 /sys/class/hw_power/power_ui/water_status
    chown system system /sys/class/hw_power/power_ui/water_status
    chmod 0444 /sys/class/hw_power/power_ui/heating_status
    chown system system /sys/class/hw_power/power_ui/heating_status
    chmod 0660 /sys/class/hw_power/interface/adap_volt
    chown system system /sys/class/hw_power/interface/adap_volt
    chmod 0660 /sys/class/hw_power/interface/wl_thermal_ctrl
    chown system system /sys/class/hw_power/interface/wl_thermal_ctrl
    chmod 0220 /sys/class/hw_power/interface/iin_thermal
    chown system system /sys/class/hw_power/interface/iin_thermal
    chmod 0660 /sys/class/hw_power/interface/ichg_thermal
    chown system system /sys/class/hw_power/interface/ichg_thermal
    chmod 0440 /sys/class/hw_power/interface/vbus
    chown system system /sys/class/hw_power/interface/vbus

    chmod 0220 /sys/class/hw_power/battery/battery_ocv/update_ocv_table
    chown system system /sys/class/hw_power/battery/battery_ocv/update_ocv_table

    chmod 0660 /sys/class/hw_power/charger/charge_data/iin_thermal
    chown system system /sys/class/hw_power/charger/charge_data/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger/adaptor_voltage
    chown system system /sys/class/hw_power/charger/direct_charger/adaptor_voltage

    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc4/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger_sc4/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc4/set_resistance_threshold
    chown system system /sys/class/hw_power/charger/direct_charger_sc4/set_resistance_threshold
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc4/set_chargetype_priority
    chown system system /sys/class/hw_power/charger/direct_charger_sc4/set_chargetype_priority
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/iin_thermal
    chown system system /sys/class/hw_power/charger/direct_charger_sc/iin_thermal
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/set_resistance_threshold
    chown system system /sys/class/hw_power/charger/direct_charger_sc/set_resistance_threshold
    chmod 0660 /sys/class/hw_power/charger/direct_charger_sc/set_chargetype_priority
    chown system system /sys/class/hw_power/charger/direct_charger_sc/set_chargetype_priority

    chmod 0440 /sys/class/hw_power/charger/direct_charge_turbo/turbo_support
    chown system system /sys/class/hw_power/charger/direct_charge_turbo/turbo_support
    chmod 0660 /sys/class/hw_power/charger/direct_charge_turbo/turbo_charge_status
    chown system system /sys/class/hw_power/charger/direct_charge_turbo/turbo_charge_status

    chmod 0660 /sys/class/hw_power/charger/wireless_sc/iin_thermal
    chown system system /sys/class/hw_power/charger/wireless_sc/iin_thermal
    chmod 0444 /sys/class/hw_power/wireless/tx_bd_info
    chown system system /sys/class/hw_power/wireless/tx_bd_info
    chmod 0660 /sys/devices/virtual/hw_power/power_mesg/powerct
    chown system system /sys/devices/virtual/hw_power/power_mesg/powerct
    chmod 0660 /sys/devices/virtual/hw_power/power_mesg/runtime
    chown system system /sys/devices/virtual/hw_power/power_mesg/runtime
    chmod 0440 /sys/devices/platform/huawei_batt_info/ic_id
    chown system system /sys/devices/platform/huawei_batt_info/ic_id
    chmod 0440 /sys/devices/platform/huawei_batt_info/batt_id
    chown system system /sys/devices/platform/huawei_batt_info/batt_id
    chmod 0440 /sys/devices/platform/huawei_batt_info/ic_status
    chown system system /sys/devices/platform/huawei_batt_info/ic_status
    chmod 0440 /sys/devices/platform/huawei_batt_info/sn_status
    chown system system /sys/devices/platform/huawei_batt_info/sn_status
    chmod 0440 /sys/devices/platform/huawei_batt_info/key_status
    chown system system /sys/devices/platform/huawei_batt_info/key_status
    chmod 0440 /sys/devices/platform/huawei_batt_info/official
    chown system system /sys/devices/platform/huawei_batt_info/official
    chmod 0440 /sys/devices/platform/huawei_batt_info/board
    chown system system /sys/devices/platform/huawei_batt_info/board
    chmod 0440 /sys/devices/platform/huawei_batt_info/battery
    chown system system /sys/devices/platform/huawei_batt_info/battery

    chmod 0660 /sys/class/hw_power/interface/disable_function
    chown system system /sys/class/hw_power/interface/disable_function
    chmod 0660 /sys/class/hw_power/soc_control/control
    chown system system /sys/class/hw_power/soc_control/control
    chmod 0220 /sys/devices/platform/huawei_batt_info/check_request
    chown system system /sys/devices/platform/huawei_batt_info/check_request
    chmod 0440 /sys/devices/platform/huawei_batt_info/check_execute_state
    chown system system /sys/devices/platform/huawei_batt_info/check_execute_state
    chmod 0660 /sys/devices/platform/huawei_batt_info/battery_permit
    chown system system /sys/devices/platform/huawei_batt_info/battery_permit
    chmod 0440 /sys/devices/platform/huawei_batt_info/can_check_in_running
    chown system system /sys/devices/platform/huawei_batt_info/can_check_in_running
    chmod 0220 /sys/devices/platform/huawei_batt_info/powerct_error_code
    chown system system /sys/devices/platform/huawei_batt_info/powerct_error_code
    chmod 0660 /sys/devices/platform/huawei_batt_info/final
    chown system system /sys/devices/platform/huawei_batt_info/final

    chmod 0440 /sys/devices/platform/huawei_batt_soh/basp/basp_ichg_max
    chown system system /sys/devices/platform/huawei_batt_soh/basp/basp_ichg_max
    chmod 0440 /sys/devices/platform/huawei_batt_soh/iscd/iscd_data
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_data
    chmod 0440 /sys/devices/platform/huawei_batt_soh/iscd/iscd_process_event
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_process_event
    chmod 0440 /sys/devices/platform/huawei_batt_soh/iscd/iscd_imonitor_data
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_imonitor_data
    chmod 0440 /sys/devices/platform/huawei_batt_soh/iscd/iscd_battery_current_avg
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_battery_current_avg
    chmod 0660 /sys/devices/platform/huawei_batt_soh/iscd/iscd_uevent_notify
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_uevent_notify
    chmod 0660 /sys/devices/platform/huawei_batt_soh/iscd/iscd_dmd
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_dmd
    chmod 0660 /sys/devices/platform/huawei_batt_soh/iscd/iscd_status
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_status
    chmod 0660 /sys/devices/platform/huawei_batt_soh/iscd/iscd_limit_support
    chown system system /sys/devices/platform/huawei_batt_soh/iscd/iscd_limit_support
    chmod 0660 /sys/devices/platform/huawei_batt_soh/bsoh_imonitor_report_interval
    chown system system /sys/devices/platform/huawei_batt_soh/bsoh_imonitor_report_interval
    chmod 0660 /sys/devices/platform/huawei_batt_soh/bsoh_sysfs_notify
    chown system system /sys/devices/platform/huawei_batt_soh/bsoh_sysfs_notify
    chmod 0660 /sys/devices/platform/huawei_batt_soh/bsoh_dmd_report
    chown system system /sys/devices/platform/huawei_batt_soh/bsoh_dmd_report
    chmod 0440 /sys/devices/platform/huawei_batt_soh/bsoh_battery_removed
    chown system system /sys/devices/platform/huawei_batt_soh/bsoh_battery_removed
    chmod 0440 /sys/devices/platform/huawei_batt_soh/bsoh_subsys
    chown system system /sys/devices/platform/huawei_batt_soh/bsoh_subsys

    trigger cust_parse_action
    trigger load_system_props_action
    class_start charger
#    wait /dev/block/bootdevice/by-name/oeminfo
    start oeminfo_nvm
    start teecd
    mkdir /mnt/vendor/nvcfg
#    mount ext4 /dev/block/platform/bootdevice/by-name/nvcfg /mnt/vendor/nvcfg rw wait
    chown system system /mnt/vendor/nvcfg
    chmod 0771 /mnt/vendor/nvcfg
    restorecon_recursive /mnt/vendor/nvcfg
    write /sys/power/pm_async 0
    load_usb_mode_cfg
#    start hvdcp_opti
    start power_off_alarm
    start bms_event
    start powerct
    start bsoh
    start console
    wait_for_prop apexd.status activated
    perform_apex_config
#    mount_all --late

service console /system/bin/sh
    class core
    console
    disabled
    user root
    group shell log readproc
    seclabel u:r:shell:s0
    setenv HOSTNAME console

service ueventd /system/bin/ueventd
    critical
    seclabel u:r:ueventd:s0

service charger /system/bin/bms_animation
    class charger
    user root
    group root system
    critical
    seclabel u:r:charger:s0

service apexd /system/bin/apexd
    interface aidl apexservice
    class core
    user root
    group system
    oneshot
    disabled # does not start with the core class
    reboot_on_failure reboot,apexd-failed

service apexd-bootstrap /system/bin/apexd --bootstrap
    user root
    group system
    oneshot
    disabled
    reboot_on_failure reboot,bootloader,bootstrap-apexd-failed

service apexd-charger /system/bin/apexd --charger
    user root
    group system
    oneshot
    disabled

on property:sys.usb.config=manufacture,adb && property:sys.usb.configfs=1 && property:vendor.usb.charger_mode=1
    write /config/usb_gadget/g1/idVendor 0x12d1
    write /config/usb_gadget/g1/idProduct 0x107d
    start adbd

on property:sys.usb.ffs.ready=1 && property:sys.usb.config=adb && property:sys.usb.configfs=1
    write /config/usb_gadget/g1/configs/b.1/strings/0x409/configuration "adb"
    rm /config/usb_gadget/g1/configs/b.1/f1
    rm /config/usb_gadget/g1/configs/b.1/f2
    rm /config/usb_gadget/g1/configs/b.1/f3
    rm /config/usb_gadget/g1/configs/b.1/f4
    rm /config/usb_gadget/g1/configs/b.1/f5
    rm /config/usb_gadget/g1/configs/b.1/f6
    rm /config/usb_gadget/g1/configs/b.1/f7
    rm /config/usb_gadget/g1/configs/b.1/f8
    symlink /config/usb_gadget/g1/functions/ffs.adb /config/usb_gadget/g1/configs/b.1/f1
    write /sys/devices/virtual/android_usb/android0/port_mode 12
    write /config/usb_gadget/g1/UDC ${sys.usb.controller}
    setprop sys.usb.state ${sys.usb.config}

# /* mount decrypt data */
on property:vold.decrypt=trigger_default_encryption
    start defaultcrypto

on nonencrypted
    trigger data_ready

on data_ready
    setprop sys.userdata_is_ready 1
    load_persist_props
    start xlogctl_service

on post-fs-data
    setprop vold.post_fs_data_done 1

on userspace-reboot-requested
    setprop apexd.status ""

on property:vold.decrypt=trigger_restart_framework
    trigger data_ready

on property:vold.decrypt=trigger_post_fs_data
    trigger post-fs-data

on cust_parse_action
    cust_parse

on load_system_props_action
    load_system_props

#service vold /system/bin/vold \
#        --blkid_context=u:r:blkid:s0 --blkid_untrusted_context=u:r:blkid_untrusted:s0 \
#        --fsck_context=u:r:fsck:s0 --fsck_untrusted_context=u:r:fsck_untrusted:s0
#    class core
#    socket vold stream 0660 root mount
#    socket cryptd stream 0660 root mount
#    ioprio be 2

service defaultcrypto /system/bin/vdc --wait cryptfs mountdefaultencrypted
    disabled
    oneshot

service teecd /sbin/teecd
    disabled
    user root
    group root
    seclabel u:r:tee:s0

# /*The service, trigger, persist  associated with the log */
service xlogctl_service /sbin/hilogcat-early -t 0
    class late_start
    user root
    group system
    disabled
    oneshot
    seclabel u:r:xlogcat:s0

service xlogview_service /sbin/hilogcat-early -t 2
    class late_start
    user root
    group system
    disabled
    oneshot
    seclabel u:r:xlogcat:s0

service shlogd /system/vendor/bin/shs
    class late_start
    user root
    group system
    disabled

service kmsglogcat /sbin/hilogcat-early -b kmsglogcat
    class late_start
    user root
    group system
    disabled
    seclabel u:r:xlogcat:s0

service oeminfo_nvm /vendor/bin/oeminfo_nvm_server
    group system readproc root
    critical
    ioprio rt 4
    seclabel u:r:oeminfo_nvm:s0

service bms_event /vendor/bin/bms_event -w 0
    user system
    group system
    disabled

on property:vendor.bms_event=switch
    restart charger

service powerct /vendor/bin/powerct -s
    user system
    group system
    disabled

service bsoh /vendor/bin/bsoh
    user system
    group system
    disabled

#service adbd /system/bin/adbd --root_seclabel=u:r:su:s0
#    disabled
#    socket adbd stream 660 system system
#    seclabel u:r:adbd:s0

service adbd /apex/com.android.adbd/bin/adbd --root_seclabel=u:r:su:s0
    class core
    socket adbd seqpacket 660 system system
    disabled
    override
    seclabel u:r:adbd:s0

on property:persist.sys.ssr.enable_debug=*
    write /sys/module/subsys_pil_tz/parameters/enable_debug ${persist.sys.ssr.enable_debug}

on property:persist.sys.mba_boot_timeout=*
    write /sys/module/pil_msa/parameters/pbl_mba_boot_timeout_ms ${persist.sys.mba_boot_timeout}

on property:persist.sys.modem_auth_timeout=*
    write /sys/module/pil_msa/parameters/modem_auth_timeout_ms ${persist.sys.modem_auth_timeout}

on property:persist.sys.pil_proxy_timeout=*
    write /sys/module/peripheral_loader/parameters/proxy_timeout_ms ${persist.sys.pil_proxy_timeout}

service power_off_alarm /vendor/bin/power_off_alarm
    class core
    group system
    disabled

service hvdcp_opti /system/vendor/bin/hvdcp_opti
    class main
    user root
    group system wakelock
