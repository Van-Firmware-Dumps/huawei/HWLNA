version : huawei.wakeup.v.6.0.2
committer : zhengwei
date : 2023.6.1
description :HM4.0唤醒性能优化以及回退oneshot三级

version : huawei.wakeup.v.5.3.5
committer : chenyidan
date : 2023.3.6
description :注册语速慢和大声提示判断

version : huawei.wakeup.v.5.3.4
committer : chenyidan
date : 2023.1.30
description :放宽音素门限的个

version : huawei.wakeup.v.5.3.3
committer : panyang
date : 2022.9.9
description :增加自噪一级E2E

version : huawei.wakeup.v.5.3.2
committer : panyang
date : 2022.9.5
description :驾驶增加E2E_GRU降低门限

version : huawei.wakeup.v.5.3.1
committer : panyang
date : 2022.8.30
description :自噪门限修改

version : huawei.wakeup.v.5.3.0
committer : zhengwei
date : 2022.4.25
description :HM3.0唤醒优化 

version : huawei.wakeup.v.5.2.0
committer : zhengwei
date : 2021.7.21
description :支持自定义唤醒词 

version : huawei.wakeup.v.3.1.2
committer : zhengwei
date : 2020.1.6
description : 驾驶模式优化

version : huawei.wakeup.v.3.1.1
committer : zhengwei
date : 2019.12.14
description : support npu逃生

version : huawei.wakeup.v.3.1.0
committer : zhengwei
date : 2019.11.20
description : phoenix c20 wakeup3.1

version : huawei.wakeup.v.3.0.2
committer : zhengwei
date : 2019.08.01
description : phoenix c10 AEC优化

version : huawei.wakeup.v.3.0.1
committer : zhengwei
date : 2019.07.25
description : phoenix c10 增加xvector和GMM融合方案

version : huawei.wakeup.v.3.0.0
committer : zhengwei
date : 2019.05.06
description : phoenix c10 wakeup3.0

version : huawei.wakeup.v.1.2.1.0
committer : zhengwei
date : 2018.11.23
description : atlanta c20 wakeup2.1

version : huawei.wakeup.v.1.0.3
committer : zhengwei
date : 2018.11.23
description : 你好YOYO修改

version : huawei.wakeup.v.1.0.2
committer : guzhengming
date : 2018.06.27
description : wakeup2.0 enhance

version : huawei.wakeup.v.1.0.1
committer : wangcong
date : 2018.05.16
description : p20 wakeup enhance

version : huawei.wakeup.v.1.0.0
committer : huangli
date : 2017.10.14
description : new
